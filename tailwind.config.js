// See https://tailwindcss.com/docs/configuration for details

module.exports = {
  theme: {
    extend: {
      spacing: {
        "72": `18rem`
      }
    },
    colors: {
      "indigo-50": `#E8EAF6`,
      "indigo-100": `#C5CAE9`,
      "indigo-200": `#9FA8DA`,
      "indigo-300": `#7986CB`,
      "indigo-400": `#5C6BC0`,
      "indigo-500": `#3F51B5`,
      "indigo-600": `#3949AB`,
      "indigo-700": `#303F9F`,
      "indigo-800": `#283593`,
      "indigo-900": `#1A237E`,
      "indigo-A100": `#8C9EFF`,
      "indigo-A200": `#536DFE`,
      "indigo-A400": `#3D5AFE`,
      "indigo-A700": `#304FFE`,

      "gray-50": `#ebeff2`,
      "gray-100": `#e2e8ed`,
      "gray-200": `#d8e0e5`,
      "gray-300": `#c5d1d8`,
      "gray-400": `#b6c3cc`,
      "gray-500": `#a3b5bf`,
      "gray-600": `#889aa5`,
      "gray-700": `#677e8c`,
      "gray-800": `#425866`,
      "gray-900": `#23343f`,

      black: `#000000`,
      white: `#FFFFFF`
    },

    fontFamily: {
      sans: [
        `lato-light`,
        `system-ui`,
        `-apple-system`,
        `BlinkMacSystemFont`,
        `"Segoe UI"`,
        `Roboto`,
        `"Helvetica Neue"`,
        `Arial`,
        `"Noto Sans"`,
        `sans-serif`,
        `"Apple Color Emoji"`,
        `"Segoe UI Emoji"`,
        `"Segoe UI Symbol"`,
        `"Noto Color Emoji"`
      ],
      serif: [
        `roboto_slabregular`,
        `Georgia`,
        `Cambria`,
        `"Times New Roman"`,
        `Times`,
        `serif`
      ],
      mono: [
        `Menlo`,
        `Monaco`,
        `Consolas`,
        `"Liberation Mono"`,
        `"Courier New"`,
        `monospace`
      ],
      heading: [
        `lato-medium`,
        `-apple-system`,
        `BlinkMacSystemFont`,
        `"Segoe UI"`,
        `Roboto`,
        `"Helvetica Neue"`,
        `Arial`,
        `"Noto Sans"`,
        `sans-serif`,
        `"Apple Color Emoji"`,
        `"Segoe UI Emoji"`,
        `"Segoe UI Symbol"`,
        `"Noto Color Emoji"`
      ],
      regular: [
        `lato-regular`,
        `-apple-system`,
        `BlinkMacSystemFont`,
        `"Segoe UI"`,
        `Roboto`,
        `"Helvetica Neue"`,
        `Arial`,
        `"Noto Sans"`,
        `sans-serif`,
        `"Apple Color Emoji"`,
        `"Segoe UI Emoji"`,
        `"Segoe UI Symbol"`,
        `"Noto Color Emoji"`
      ]
    }
  },
  variants: {},
  plugins: []
};
