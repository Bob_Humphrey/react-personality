import React, { useContext } from "react";
import { TestContext } from "../context/TestContext";
import PreferenceAnswers from "./PreferenceAnswers";

const Answers = props => {
  const { state } = useContext(TestContext);
  return (
    <div className="w-full">
      <div className="w-full">
        <div className="lg:flex w-full text-base font-heading font-bold justify-center mb-2 ">
          Your Answers
        </div>
      </div>
      <div className="w-full">
        <PreferenceAnswers preferences={state.preferences[0]} />
      </div>
      <div className="w-full">
        <PreferenceAnswers preferences={state.preferences[1]} />
      </div>
      <div className="w-full">
        <PreferenceAnswers preferences={state.preferences[2]} />
      </div>
      <div className="w-full">
        <PreferenceAnswers preferences={state.preferences[3]} />
      </div>
    </div>
  );
};

export default Answers;
