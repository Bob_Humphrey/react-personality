import React, { useContext } from "react";
import { TestContext } from "../context/TestContext";
import Answer from "./Answer";

const PreferenceAnswers = props => {
  const { state } = useContext(TestContext);
  if (props.preferences.show === false) {
    return null;
  }
  return (
    <div className="w-full px-16">
      <div className="lg:flex w-full text-base font-heading font-bold justify-center mt-6">
        {props.preferences.preference1} / {props.preferences.preference2}
      </div>
      <div className=" flex w-full justify-center font-sans text-sm uppercase text-gray-500 px-6 pb-1">
        <div className="w-1/4">Description</div>
        <div className="w-1/4 flex items-center justify-center">
          Your Choice
        </div>
        <div className="w-1/4">Description</div>
        <div className="flex justify-between w-1/4">
          <div className="flex justify-start">Preference</div>
          <div className="flex justify-end">Points</div>
        </div>
      </div>
      <div className="w-full border-b border-gray-200">
        {state.questions.map(function(question, index) {
          if (
            props.preferences.abbreviation === question.measure &&
            question.score > 0
          ) {
            return (
              <div className="flex w-full" key={index}>
                <Answer question={question} />
              </div>
            );
          } else {
            return null;
          }
        })}
      </div>
    </div>
  );
};

export default PreferenceAnswers;
